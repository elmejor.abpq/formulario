const index= document.querySelector('#index')
index.addEventListener('submit', (e)=>{
e.preventDefault()

const cedula = document.querySelector('#cedula').value
const apellidos = document.querySelector('#apellidos').value
const nombres = document.querySelector('#nombres').value
const direccion = document.querySelector('#direccion').value
const celular = document.querySelector('#celular').value
const correo = document.querySelector('#correo').value

const Clientes = JSON.parse(localStorage.getItem('Clientes')) || []
const isClienteRegistered = Clientes.find(Cliente => Clientes.cedula ==cedula)
if(isClienteRegistered){
   return alert('El cliente ya esta registrado')
}

Clientes.push({ cedula: cedula, apellidos: apellidos, nombres: nombres, direccion: direccion,
   celular: celular, correo: correo})
   localStorage.setItem('Clientes', JSON.stringify(Clientes))  
   alert ('Registro Exitoso!')
   window.location.href= 'index.html'
})